/*
 * Circle.h
 *
 *		@date 15.11.20
 *      @author: 152120171059
 *		@brief: circle header file
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:
	Circle(double);
	virtual ~Circle();
	void setR(double);
	void setR(int);
	double getR() const;
	double calculateCircumference() const;
	double calculateArea();
	bool equals(Circle);
private:
	double r;
	const double PI = 3.14; 
};
#endif /* CIRCLE_H_ */