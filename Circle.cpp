/*
 * Circle.cpp
 *
 *		@date 15.11.20
 *      @author: 152120171059
 *		@brief: functions of Circle class
 */


#include "Circle.h"

    Circle::Circle(double r) {
        setR(r);
    }

    Circle::~Circle() {
    }

    void Circle::setR(double r) {
        this->r = r;
    }
    void Circle::setR(int r) {
        this->r = r;
    }

    double Circle::getR() const {
        return r;
    }

    double Circle::calculateCircumference() const {
        return PI * r * 2;
    }

    double Circle::calculateArea() {
        return PI * r * r;
    }

    bool  Circle::equals(Circle c1) {
        if (getR() == c1.getR())
            return true;
        else
            return false;
    }