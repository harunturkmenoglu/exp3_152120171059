/*
 * Square.h
 *
 *		@date 15.11.20
 *      @author: 152120171059
 *		@brief: square header file
 */

#ifndef SQUARE_H_
#define SQUARE_H_

class Square {
public:
	Square(double);
	virtual ~Square();
	void setA(double);
	void setB(double);
	double calculateCircumference();
	double calculateArea();
private:
	double a;
	double b;
};

#endif /* SQUARE_H_ */